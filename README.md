## [WORKSHOP] python et pygame

## Introduction au Python

```python
#!/usr/bin/env python3

class Car:
    # the __init__ is called when the class is created
    def __init__(self, fuel, name):
        self.fuel = fuel
        self.name = name

    def drive(self):
        # we remove 5 from the car's fuel
        self.fuel -= 5

    def change_name(self, new_name):
        self.name = new_name


# create an instance of the car class
Toyota = Car(100, "Toytota 500")

# call the function "drive"
Toyota.drive()

# call the function "change_name"
Toyota.change_name("Peugeot")

# display the car's fuel
print(Toyota.fuel)

# if condition:
if Toyota.fuel > 10 :
  print(True)

# while condition:
while Toyota.fuel > 50 :
  Toyota.drive()

print(Toyota.fuel)

# for iterator:
for x in range(1,5):
  print(x)
```

## Introduction à Pygame

•	Création de la fenêtre

```python
#!/usr/bin/env python3

import pygame

BACKGROUND = 0, 0, 0

class Game():
  def __init__(self):
        pygame.init()
        # Change the window's title
        pygame.display.set_caption("WORKSHOP")
        # Change the window's size
        self.screen = pygame.display.set_mode((1000, 600))
        # change the background color of the window
        self.screen.fill(BACKGROUND)
        self.running = True

#launch the game :
game = Game()
```

•	Boucle de jeu 

Avec seulement le code du dessus, la fenêtre va s’ouvrir et se refermer immédiatement.
La fonction suivante va permettre de garder la fenêtre ouverte :

```python
  def run(self):
        while self.running:
            pygame.display.flip() # Rafraichi l'image
```

n'oubliez pas d'appeler la fonction avec ```game.run() ```

Vous devriez maintenant avoir une fenêtre noire, qui reste ouverte mais que vous ne pouvez pas fermer.

•	Events

Ajoutez dans votre classe Game, la fonction suivante :

```python
  def readEvents(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
```

Ajoutez au début de la boucle de jeu la ligne suivante :

```python
self.readEvents()
```

vous pouvez maintenant fermer le programme en cliquant sur la croix.

•	Sprite


```python
class Player():
    def __init__(self):
        self.sprite = pygame.image.load("sprite.png").convert_alpha()
        self.width = self.sprite.get_width()
        self.height = self.sprite.get_height()
        self.x = 0
        self.y = 0
```

il faut appeler la classe pour créer le personnage.

```python
self.player = Player() # création du personnage
```

Puis il faut rajouter une fonction qui permettra d'afficher le sprite dans la classe Game.

```python
def display(self):
        self.screen.fill(BACKGROUND)
        self.screen.blit(self.player.sprite, (self.player.x, self.player.y))
```

et appeler cette fonction dans la boucle de jeu :

```python
self.display()
```

## maintenant que vous avez réussi à récuperer des inputs et afficher un sprite à vous de jouer !

# essayer de créer votre snake

PYGAME DOC : https://www.pygame.org/docs/

1.  Essayer de faire un snake d'une taille de 4  

tips : les tableaux peuvent êtres utiles !

2.  Essayez ensuite de le faire bouger

tips : https://www.pygame.org/docs/ref/key.html

https://www.pygame.org/docs/ref/time.html

3.  Essayer de mettre des bordures a votre map

4.  Essayer de faire mourrir votre snake quand il se touche lui même

5.  Essayer de faire apparaitre des pommes qui feront grandir votre snake

tips : regardez la lib random

# Pour aller plus loin

essayer d'implémenter un score.

essayer de rajouter des murs au mileu de la map pour augmenter la difficulté.

essayez de générer des maps de façon aléatoires.

